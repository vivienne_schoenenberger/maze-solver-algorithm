(function(window)
{

    'use strict';

    /**
     * Maze class
     * @param wrapper
     * @param button
     * @param solve_button
     */
    function ThreeMaze(wrapper, button, solve_button)
    {
        // Object attributes
        this.wrapper = wrapper;
        this.camera = {};
        this.cameraHelper = {};
        this.scene = {};
        this.materials = {};
        this.map = [];
        
        this.renderer = {};
        this.player = {};
        this.end = {};
        this.length = 21;
        this.thickness = 20;
        this.new_map = [];
        this.myMaze;
        this.maze;
        this.binary_maze;
        this.new_tween;
        this.sol = [];
        this.mergedMap = [];
        this.originalMap = [];

        // Inits
        this.initScene();
        this.onWindowResize();
        this.render();

        // Events
        this.wrapper.addEventListener('mousemove', this.onMouseMove.bind(this));
        this.wrapper.addEventListener('mousedown', this.onMouseDown.bind(this));
        this.wrapper.addEventListener('mouseup', this.onMouseUp.bind(this));
        button.addEventListener('click',this.onGenerateMaze.bind(this));
        button.dispatchEvent(new Event('click'));

        solve_button.addEventListener('click',  this.solveMazeOnClick.bind(this));
        window.addEventListener('resize', this.onWindowResize.bind(this));
        document.addEventListener('keydown', this.onKeyDown.bind(this));
    };

    /**
     * Generates a new maze
     * Loops into the maze, removes old blocks and adds new ones
     * 
     *
     */

    ThreeMaze.prototype.onGenerateMaze = function()
    {
        this.new_map = this.generateMaze(this.length);
        this.originalMap = this.generateMaze(this.length);
        var new_player_path = [];


       
        var latency = 100;
        var self = this;
        var tween = null;
        
        for (var x = this.length; x > 0; x -= 1)
        {
            new_player_path[x] = [];
            
            for (var y = 1; y < this.length + 1; y += 1)
            {
                var delay = ((this.length - x) * latency) + ((this.length - y) * latency);
                // Inits player path
                new_player_path[x][y] = false;
                // Removes old mesh if needed

                

                if (typeof this.map[x] != 'undefined' && typeof this.map[x][y] != 'undefined' && typeof this.map[x][y] === 'object'  )
                {   
                    tween = new TWEEN.Tween({scale: 1, y: this.thickness / 2, mesh: this.map[x][y]}).to({scale: 0, y: 0}, 200).delay(delay);
                    tween.onUpdate(this.onUpdateTweeningMesh);
                    tween.onComplete(function()
                    {
                        this.mesh.visible = false;
                        self.scene.remove(this.mesh);
                    });
                    tween.start();

                }


                // Removes player path if needed
                if (typeof this.player.path != 'undefined' && typeof this.player.path[x] != 'undefined' && typeof this.player.path[x][y] != 'undefined' && typeof this.player.path[x][y] === 'object')
                {
                    this.removePlayerPath(x, y, delay);
                }
                // Removes solve path if needed
                if (typeof this.sol != 'undefined' && typeof this.sol[x] != 'undefined' && typeof this.sol[x][y] != 'undefined' && typeof this.sol[x][y] === 'object')
                {
                    this.removeSolvePath(x, y, delay);
                }

                // Adds a new mesh if needed
                if (this.new_map[x][y] === 0)
                {
                    

                    // Generates the mesh
                    var wall_geometry = new THREE.CubeGeometry(this.thickness, this.thickness, this.thickness, 1, 1, 1);
                    this.new_map[x][y] = new THREE.Mesh(wall_geometry, this.materials.grey);
                    this.new_map[x][y].visible = false;
                    this.new_map[x][y].position.set(x * this.thickness - ((this.length * this.thickness) / 2), 0, y * 20 - ((this.length * this.thickness) / 2));
                    this.scene.add(this.new_map[x][y]);
   
                    // Builds the related tween
                    tween = new TWEEN.Tween({scale: 0, y: 0, mesh: this.new_map[x][y]}).to({scale: 1, y: this.thickness / 2}, 300).delay(delay);
                    tween.onUpdate(this.onUpdateTweeningMesh);
                    tween.onStart(function()
                    {
                        this.mesh.visible = true;
                    });
                    
                    
                    tween.start();
                }

                else
                {
                    this.new_map[x][y] = false;
                    
                }
                
            }
            }


        // Animates the end block
        var end_hide_tween = new TWEEN.Tween({scale: 1, y: this.thickness / 2, mesh: this.end}).to({scale: 0, y: 0}, 300);
        var end_show_tween = new TWEEN.Tween({scale: 0, y: 0, mesh: this.end}).to({
            scale: 1,
            y: this.thickness / 2
        }, 300).delay((this.length * 2) * latency);
        end_hide_tween.onUpdate(this.onUpdateTweeningMesh);
        end_show_tween.onUpdate(this.onUpdateTweeningMesh);
        end_show_tween.onStart(function()
        {
            this.mesh.visible = true;
        });
        end_hide_tween.onComplete(function()
        {
            this.mesh.visible = false;
        });
        if (this.end.scale != 0)
        {
            end_hide_tween.start();
        }
        end_show_tween.start();
   

        this.map = this.new_map;
        this.player.path = new_player_path;
        // Inits player
        this.player.mazePosition = {x: this.length - 1, z: this.length - 1};
        this.movePlayer(false);
        //this.solveMazeOnClick();
    };


    /**
     * call solve function
     */
     ThreeMaze.prototype.solveMazeOnClick = function(){
        
         this.binary_maze = this.makeBinaryMaze()
         this.solveMaze();



     }

    /**
     * convert maze-array to binary
     */

    ThreeMaze.prototype.makeBinaryMaze = function(){
       

        var binaryArray =Array.from(Array(22), () => new Array(22))
        console.log(binaryArray)
        for (var x= 1 ; x<this.length+1; x++)
        {

           
           
            for (var y = 1 ; y<this.length+1; y++)
            {
                

                if (this.new_map[x][y] === false){


                    binaryArray[x][y] = 1;
                }
                else {
                   
                    binaryArray[x][y] = 0;
                }
            }
            
           
        }
        
        return binaryArray;

        
    };

    

/* This function solves the Maze problem using
    Backtracking. It mainly uses solveMazeUtil()
    to solve the problem. It returns false if no
    path is possible, otherwise return true and
    prints the path in the form of 1s. */


     ThreeMaze.prototype.solveMaze = function(){
        
        for(let i=1;i< this.length +1 ;i++)
        {
            this.sol[i]=new Array(this.length);
            for(let j=1;j<this.length +1;j++)
            {
                this.sol[i][j]= 0;
            }
        }
        if (this.solveMazeUtil(2,2)== false) {

            console.log("------no solution-----")
            
            return false;
            
        }  
       
        return true;


     }

     ThreeMaze.prototype.isSafe = function(x,y){
         return(x >=2 && x < this.length && y >= 2 && y < this.length && this.binary_maze[x][y]== 1);
     }


     /* A recursive utility function to solve Maze
    problem */

     ThreeMaze.prototype.solveMazeUtil = function(x,y){

      
         // if (x, y is goal) return true
        
         if (x ==this.length-1  && y == this.length-1
            && this.binary_maze[x][y] == 1) {
                this.sol[x][y] = 1;
            
            this.printSolution(x,y);
            
            return true;
        }
        

  
        // Check if maze[x][y] is valid
        if (this.isSafe(x, y) == true) {
        // Check if the current block is already part of solution path. 
        if (this.sol[x][y] == 1){
                    return false;
                
                }
          // mark x, y as part of solution path
            this.sol[x][y] = 1;
       
            // mark x, y as part of solution path
            this.sol[x][y]= 1;
  
            /* Move forward in x direction */
            if (this.solveMazeUtil(x + 1, y)){
                return true;
            }
  
            /* If moving in x direction doesn't give
            solution then Move down in y direction */
            if (this.solveMazeUtil( x, y + 1)){
                return true;
            }
            
            /* If moving in y direction doesn't give
            solution then Move backwards in x direction */
            if (this.solveMazeUtil( x - 1, y)){
            return true;}
  
            /* If moving backwards in x direction doesn't give
            solution then Move upwards in y direction */
            if (this.solveMazeUtil( x, y - 1)){
                return true;
            }


            /* If none of the above movements works then
            BACKTRACK: unmark x, y as part of solution
            path */
            this.sol[x][y] = 0;
            return false;

            

     }
     
     return false;
    }




    ThreeMaze.prototype.printSolution = function(x,y){
    for (var x = this.length; x > 0; x -= 1)
    {

    for (var y = 1; y < this.length + 1; y += 1){

    if(this.sol[x][y]=== 1 ){
    
     // Generates the mesh
     var wall_geometry2 = new THREE.CubeGeometry(this.thickness, this.thickness, this.thickness, 1, 1, 1);
     this.sol[x][y]= new THREE.Mesh(wall_geometry2, this.materials.green);
     this.sol[x][y].visible = true;
     this.sol[x][y].position.set(x * this.thickness - ((this.length * this.thickness) / 2), 0, y * 20 - ((this.length * this.thickness) / 2));
     this.scene.add(this.sol[x][y]);
    // Builds the related tween
     var tweenSolve = new TWEEN.Tween({scale: 0, y: this.thickness * 5, mesh: this.sol[x][y]}).to({
        scale: 1,
        y: this.thickness / 8
        }, 300);
        tweenSolve.onUpdate(function()
        {
            this.mesh.scale.set(this.scale, this.scale, this.scale);
            this.mesh.position.y = this.y;
;
        });
        tweenSolve.start();

    }
    else if (this.sol[x][y]===0) {
        this.sol[x][y] = false;
    }
}

    }

}
    /**
     * Updates a mesh when doing a tween
     */
    ThreeMaze.prototype.onUpdateTweeningMesh = function()
    {
        this.mesh.scale.y = this.scale;
        this.mesh.position.y = this.y;
    };

    /**
     * Removes a mesh from the player path
     * @param x
     * @param y
     * @param delay
     */
    ThreeMaze.prototype.removePlayerPath = function(x, y, delay)
    {
        var tween = new TWEEN.Tween({scale: 1, y: this.thickness / 8, mesh: this.player.path[x][y]}).to({
            scale: 0,
            y: this.thickness * 5
        }, 300).delay(delay);
        var self = this;
        this.player.path[x][y] = false;
        tween.onUpdate(function()
        {
            this.mesh.scale.set(this.scale, this.scale, this.scale);
            this.mesh.position.y = this.y;
        });
        tween.onComplete(function()
        {
            self.scene.remove(this.mesh);
        });
        tween.onStart(function()
        {
            this.mesh.visible = true;
        });
        tween.start();
    };

    ThreeMaze.prototype.removeSolvePath = function(x, y, delay)
    {
        var tween = new TWEEN.Tween({scale: 1, y: this.thickness / 8, mesh: this.sol[x][y]}).to({
            scale: 0,
            y: this.thickness * 5
        }, 300).delay(delay);
        var self = this;
        this.sol[x][y] = false;
        tween.onUpdate(function()
        {
            this.mesh.scale.set(this.scale, this.scale, this.scale);
            this.mesh.position.y = this.y;
        });
        tween.onComplete(function()
        {
            self.scene.remove(this.mesh);
        });
        tween.onStart(function()
        {
            this.mesh.visible = true;
        });
        tween.start();
    };

    /**
     * Inits the scene
     */
    ThreeMaze.prototype.initScene = function()
    {
        // Scene
        this.scene = new THREE.Scene();

        // Materials
        this.materials =
        {
            grey: new THREE.MeshLambertMaterial({color: 0xffffff, wireframe: false}),
            red: new THREE.MeshLambertMaterial({color: 0xf18260, ambient: 0xf18260, lineWidth: 1}),
            purple:new THREE.MeshLambertMaterial({color: 0x9A47FF, ambient: 0x9A47FF, lineWidth: 1}),
            purple2:new THREE.MeshLambertMaterial({color: 0xAC76FF, ambient: 0xAC76FF, lineWidth: 1}),
            green: new THREE.MeshLambertMaterial({color: 0x27FAA1, ambient: 0x27FAA1, lineWidth: 1}) 
        };

        // Camera
        this.camera = new THREE.PerspectiveCamera(45, 1, 1, 2000);
        this.camera.clicked = false;

        // Lights
        this.scene.add(new THREE.AmbientLight(0xc9c9c9));
        var directional = new THREE.DirectionalLight(0xc9c9c9, 0.5);
        directional.position.set(0, 0.5, 1);
        this.scene.add(directional);

        // Player
        this.player = new THREE.Object3D();
        var head_mesh = new THREE.Mesh(new THREE.SphereGeometry(this.thickness / 2, 9, 9), this.materials.purple);
        var body_mesh = new THREE.Mesh(new THREE.CylinderGeometry(this.thickness / 6, this.thickness / 2, this.thickness * 1.5, 12, 1), this.materials.purple);
        this.player.add(head_mesh);
        this.player.add(body_mesh);
        head_mesh.position.y = this.thickness * 1.5;
        body_mesh.position.y = this.thickness;
        this.scene.add(this.player);

        // End of the maze
        this.end = new THREE.Mesh(new THREE.CubeGeometry(this.thickness, this.thickness, this.thickness, 1, 1, 1), this.materials.purple);
        this.end.position.set(-((this.length / 2) * this.thickness) + (this.thickness * 2), 0, -((this.length / 2) * this.thickness) + (this.thickness * 2));
        this.end.scale.y = 0;
        this.end.visible = false;
        this.scene.add(this.end);

        // Camera helper
        var geometry = new THREE.Geometry();
        geometry.vertices.push(new THREE.Vector3(0, 0, 0), new THREE.Vector3(Math.sqrt(3) * (this.length * this.thickness)), 0, 0);
        this.cameraHelper = new THREE.Line(geometry);
        this.scene.add(this.cameraHelper);
        this.cameraHelper.visible = false;
        this.cameraHelper.targetRotation = false;
        this.cameraHelper.rotation.set(0, 1.362275, 0.694716);

        // Renderer
        this.renderer = typeof WebGLRenderingContext != 'undefined' && window.WebGLRenderingContext ? new THREE.WebGLRenderer({antialias: true}) : new THREE.CanvasRenderer({});
        this.wrapper.appendChild(this.renderer.domElement);
    };

    /**
     * Keydown action
     * @param evt
     */
    ThreeMaze.prototype.onKeyDown = function(evt)
    {
        // Gets the direction depending on the pressed key
        var code = evt.keyCode;
        var direction = {x: 0, z: 0};
        var directions =
        {
            37: {x: 1, z: 0},
            39: {x: -1, z: 0},
            38: {x: 0, z: 1},
            40: {x: 0, z: -1}
        };
        if (typeof directions[code] != 'undefined')
        {
            direction = directions[code];
        }
        else
        {
            return;
        }

        var x = this.player.mazePosition.x;
        var z = this.player.mazePosition.z;
        var target_block = this.map[x + direction.x][z + direction.z];
        if (target_block === false)
        {
            // If the player moves forward, adds a block to the path
            if (this.player.path[x + direction.x][z + direction.z] === false)
            {
                // Builds the mesh
                this.player.path[x][z] = new THREE.Mesh(new THREE.CubeGeometry(this.thickness, this.thickness / 4, this.thickness, 1, 1, 1), this.materials.purple2);
                this.player.path[x][z].position.set(-((this.length * this.thickness) / 2) + x * this.thickness, this.thickness * 5, -((this.length * this.thickness) / 2) + z * this.thickness);
                this.player.path[x][z].scale.set(0, 0, 0);
                this.scene.add(this.player.path[x][z]);

                // Builds the related tween
                var tween = new TWEEN.Tween({scale: 0, y: this.thickness * 5, mesh: this.player.path[x][z]}).to({
                    scale: 1,
                    y: this.thickness / 8
                }, 300).delay(150);
                tween.onUpdate(function()
                {
                    this.mesh.scale.set(this.scale, this.scale, this.scale);
                    this.mesh.position.y = this.y;
                });
                tween.start();
            }
            // If he goes back, removes one
            else
            {
                this.removePlayerPath(x + direction.x, z + direction.z, 0);
            }

            // Updates the player position
            this.player.mazePosition.x += direction.x;
            this.player.mazePosition.z += direction.z;
            this.movePlayer(true);
        }
    };

    /**
     * Moves the player depending on its position on the maze
     * @param animate
     */
    ThreeMaze.prototype.movePlayer = function(animate)
    {
        var from = {height: -Math.PI, x: this.player.position.x, z: this.player.position.z, mesh: this.player};
        var to = {
            height: Math.PI,
            x: -((this.length * this.thickness) / 2) + this.player.mazePosition.x * this.thickness,
            z: -((this.length * this.thickness) / 2) + this.player.mazePosition.z * this.thickness
        };
        var tween = new TWEEN.Tween(from).to(to, animate ? 300 : 0);
        var self = this;
        tween.onUpdate(function()
        {
            this.mesh.position.x = this.x;
            this.mesh.position.y = (Math.cos(this.height) + 1) * (self.thickness / 4);
            this.mesh.position.z = this.z;
        });
        // End of the maze: starts again
        tween.onComplete(function()
        {
            if (self.player.mazePosition.x === 2 && self.player.mazePosition.z === 2)
            {
                self.onGenerateMaze();
            }
        });
        tween.start();
    };

    /**
     * Moving the mouse over the container: sets a target rotation for the camera helper
     * @param evt
     */
    ThreeMaze.prototype.onMouseMove = function(evt)
    {
        if (this.camera.clicked !== false)
        {
            var target_rotation = {
                z: this.cameraHelper.rotation.z + ((evt.pageY - this.camera.clicked.y) / 800),
                y: this.cameraHelper.rotation.y + ((this.camera.clicked.x - evt.pageX) / 800)
            };
            if (target_rotation.z < 0)
            {
                target_rotation.z = 0;
            }
            if (target_rotation.z > (Math.PI / 2) - 0.1)
            {
                target_rotation.z = Math.PI / 2 - 0.1;
            }
            this.cameraHelper.targetRotation = target_rotation;
        }
    };

    /**
     * Mouse down: starts dragging the maze
     * @param evt
     */
    ThreeMaze.prototype.onMouseDown = function(evt)
    {
        evt.preventDefault();
        this.camera.clicked = {x: evt.pageX, y: evt.pageY};
    };

    /**
     * Mouse up: stops dragging the maze
     * @param evt
     */
    ThreeMaze.prototype.onMouseUp = function(evt)
    {
        evt.preventDefault();
        this.camera.clicked = false;
    };

    /**
     * Render loop
     * Sets the camera position and renders the scene
     */
    ThreeMaze.prototype.render = function()
    {
        requestAnimationFrame(this.render.bind(this));
        TWEEN.update();
        if (this.cameraHelper.targetRotation !== false)
        {
            this.cameraHelper.rotation.z += (this.cameraHelper.targetRotation.z - this.cameraHelper.rotation.z) / 10;
            this.cameraHelper.rotation.y += (this.cameraHelper.targetRotation.y - this.cameraHelper.rotation.y) / 10;
        }
        this.camera.position = this.cameraHelper.geometry.vertices[1].clone().applyProjection(this.cameraHelper.matrixWorld);
        this.camera.lookAt(this.scene.position);
        this.renderer.render(this.scene, this.camera);
    };

    /**
     * Sets the scene dimensions on window resize
     */
    ThreeMaze.prototype.onWindowResize = function()
    {
        var width = window.innerWidth || window.document.body.clientWidth;
        var height = window.innerHeight || window.document.body.clientHeight;
        this.renderer.setSize(width, height);
        this.camera.aspect = width / height;
        this.camera.updateProjectionMatrix();
    };

    window.ThreeMaze = ThreeMaze;

})(window);

